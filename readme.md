Basic usage
===

1. Prepare a CSV file with a column `ID` that contains the identifiers you want to mask.
2. Run `python generate_pseudonyms.py <path_to_your_ID_file>` to create the masking table. The program will ask for a password.
3. Use the masking table to substitute the pseudonyms in your dataset. The notebook `example_use.ipynb` shows how to do this. That part is intentionnally *not* included in the command-line interface, because *you* are responsible to keep the masking table (see next step). A way to do it with Python/pandas is included in the `example_use.ipynb` notebook.
4. If you want to generate new pseudonyms in a consistent manner (for instance because you updated the list of identifiers to mask), you must provide the same password used in step 2, as well as the salts in the previous masking table, as detailed in the section "input CSV" below.

For basic help, run `python generate_pseudonyms.py --help`. For more details, read on.

Details
===

Known limitations
---

Out-of-memory errors may occur if the input file is too large. As a rule of thumb, this should not be the case if there are fewer than ten million distinct IDs, or if the input file is smaller than 1GB.

The script tries to guess the CSV format used for the file (via `pandas.read_csv`). If the file includes a single line of a header containing column names, and uses a standard column delimiter (`,`, `;`, tabulations, etc.), you should be fine.

Input CSV
---

The input must be a CSV file:
- it must contain a column `ID` containing the identifiers to mask (such as names, phone numbers, email addresses, etc.)
- it may optionally contain a column `crypto_salt`, which will be interpreted
- it may optionally contain any number of other columns, which will be ignored.

`crypto_salt` is a cryptographic salt; each identifier must be given an associated salt to decrease the efficiency of certain deanonymisation attacks (see "cryptographic considerations", below). If you do not provide it, random values will be generated. As a result, multiple iterations of the program will generate new salts, and hence different pseudonyms. Reuse the salts from the previous iterations if you want consistency.


Password
---
You will be prompted to give a password. You _can_ leave the prompt empty or use a simple word, but do not do so unless you have read and understood the section "cryptographic considerations", below. 

If you want to regenerate pseudonyms in a consistent manner, you need to provide the same password each time. Otherwise, you do not need to keep the password.

Output format
---

The output is a three-column CSV file:
- `ID` contains the original identifiers (with any duplications removed)
- `ID_masked` contains the corresponding masked pseudonyms
- `crypto_salt` contains the cryptographic salt used for each ID.

If the input specifies a `crypto_salt` column, the same values will be used. If the column is present, but some values are empty, a random value will be generated for those values. If the input does not specify the salt values, random values will be generated. 

If the input contains multiple times the same ID, only the first occurrence is kept. If the input both has duplicate IDs _and_ provided a `crypto_salt` column, the first salt for each value will be used (consistency across lines is not checked).

The output of the script (more precisely its columns `ID` and `crypto_salt`) can be used as input of the script. In that case, and assuming the same password is reused, it is guaranteed to produce the same result.


Cryptographic considerations
===

Certain design choices made in that library are appropriate for the purpose it fullfills, but are not appropriate in other applications. The following section explains them.

Attack model
---

Security is always understood within a certain context (what is the data/process/program supposed to do, what behavior are we afraid that an attacker might induce, what resources are available to an attacker, etc.). An _attack model_ is the set of hypotheses made about what the attacker can access and what results we can tolerate, in order to proclaim a system to be "secure" or "insecure" on the basis of those hypotheses.

We make the following assumptions.

The attacker's aim is to recover at least one pre-masked identifier (and more if possible). Threats to integrity, authenticity, availability of the data are not considered, as those are supposed to be assured by the data upload channel.


We assume that the attacker has access to the full dataset of pseudonyms on their own machine. That is the case, for instance, if a data breach occurs and the pseudonymous dataset is published on the internet. Therefore, the attacker is not subject to limitations of available cracking hardware, environment, etc.

The attacker has full knowledge of the program that generates pseudonyms, as well as all parameters (such as salts) of that program [(Kerchoff's principle)](https://en.wikipedia.org/wiki/Kerckhoffs%27s_principle). The only unknown are the identifiers and (possibly, see below) a password.

We also suppose that identifiers are taken from a space of size $N$, known to the attacker. For instance, if identifiers are French mobile phone numbers (06 or 07 followed by eight random numbers), $N$ is equal to $2×10^8$. For French social security numbers (thirteen numbers, but some combinations are not allowed), a conservative lower bound for $N$ would be $10^8$ (there have been at least a hundred million registered numbers).

Notice that identifiers are usually taken from a smaller subset of the population, which may be known to the attacker. For instance, patients that visited a given hospital in a given year ($N \approx 1000-10000$) are a small subset of all French social security numbers. That subset is not known to a random internet attacker, but it may be easily accessible to hospital staff.


Dictionary attack: possible in the absence of a password
---

The attacker tries to obtain the identifier that generated a given pseudonym using a given salt.

If no password was used, the attacker can generate the pseudonym for a given identifier in $O(1)$ operations. The attacker can then compute the pseudonyms for _all_ possible identifiers in $O(N)$ for the given salt and see which one matches. Therefore, the attacker can de-anonymize one pseudonym with $O(N)$ operations.


If no salt was used, the same calculations allow to de-anonymize the full dataset in roughly[^1] $O(N)$. If a salt was used, the attacker must redo all calculations for each change of salt - therefore, in that case, the cost of deanonymizing $M$ pseudonyms is then $O(M×N)$.


[^1] after generating the pseudonym list, the attacker must still find the intersection between the generated list of size $N$ and the target list of size $M$ ($M \ll N$). This can be done in $O(N log M)$ operations: sort the $M$ targets into a red-black tree, then for each object in the generated list of size $N$, search if it exists in the tree. That is close enough to $O(N)$ for the purpose of the present analysis.

Brute-force pre-image search
---

Here we assume a strong password was used, taken from a space of size $P$.

The attacker needs to try random pairs $(ID, password)$ for one given salt and pseudonym until a match is found. Once a match is found, the same password can be reused to crack all other IDs.

The use of a password therefore increases the cost of the first attack to $O(P×N)$. Because $P$ can be very large (for instance a password generated by taking ten random letters would have $P=26^{10}\approx 1.4×10^{14}$), whereas $N$ is fixed and somewhat low (below one billion) in many applications, it substantially increases the cost of the attack. Using a weak password, or no password at all, obviously decreases the cost of the attack in proportion.

The cost of the first attack, $O(P×N)$, then dominates the cost of all other attacks, $O(M×N)$. Salting the pseudonyms is still desirable, because passwords can be leaked to the attacker (defense in depth).


See also
===

- [RFC 2104](https://datatracker.ietf.org/doc/html/rfc2104): describes the HMAC algorithm
- [NIST 800-132](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-132.pdf): recommendation for password-based key derivation
