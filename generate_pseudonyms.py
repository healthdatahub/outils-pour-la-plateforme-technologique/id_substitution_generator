import pandas as pd
import os
import argparse
import hashlib
import logging
import getpass
logger = logging.getLogger(__name__)
# logger = logging.getLogger()
from typing import Optional

# Per https://docs.python.org/3/library/hashlib.html#key-derivation:
# "The number of iterations should be chosen based on the hash algorithm and computing power. As of 2022, hundreds of thousands of iterations of SHA-256 are suggested. For rationale as to why and how to choose what is best for your application, read Appendix A.2.2 of [NIST-SP-800-132](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-132.pdf)."
# However, in our case, we are worried about attackers recovering the plaintext of a given masked text, but we do not care much about dictionary attacks. Hence, that countermeasure is not all that useful. On the other hand, we need to run one masking operation for each line of a potentially large file. Hence, we reduce it considerably (and allow the user to modify it).
DEFAULT_HMAC_ITERATIONS = 50

def rand_salt(size: int = 16) -> str:
    '''Provide a random cryptographic salt.'''
    return os.urandom(size).hex()


def _parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=f"""
Create a table of HMAC256-masked pseudonyms.

The input is a CSV file with a header of column names (and generally should be readable by Python's pandas.read_csv):
- it must contain a column "ID" containing the identifiers to mask (such as names, phone numbers, email addresses, etc.)
- optionally, it may contain a column "crypto_salt" that will be interpreted.
- any other columns can be present, but will be ignored.

The output is a three-column CSV file:
- "ID" contains the original identifiers
- "ID_masked" contains the corresponding masked pseudonyms
- "crypto_salt" contains the cryptographic salt used for each ID.

See readme and examples for more details.
"""
    )
    parser.add_argument(
        'id_file',
        type=str,
        help="Path to the file containing the original IDs. This should be a CSV file with a column named 'ID' (and possibly other columns that will be ignored)."
        )
    parser.add_argument(
        '--out',
        type=str,
        help=f"Path for the masking table. By default, it is saved as 'masking_table.csv' in the same folder as the file containing IDs."
        )
    parser.add_argument(
        '--N_iter',
        type=int,
        default = DEFAULT_HMAC_ITERATIONS,
        help=f"Number of HMAC iterations (by default: {DEFAULT_HMAC_ITERATIONS}). You must reuse the same value across iterations to obtain consistent results. Increasing it somewhat increases security against brute-force attacks, but it also increases the runtime of the program."
        )
    args = parser.parse_args()
    return args

def _parse_data_file(path: str) -> pd.DataFrame:
    """Read IDs and salts from the file, adding some if necessary."""
    
    # read the IDs
    try:
        df = pd.read_csv(
            path,
            sep=None,
            engine="python",
            keep_default_na=False,
            usecols=["ID"],
            dtype=str)
        
        input_df = df.copy()
        logger.info(f"ID column loaded from file {path}.")

    except ValueError as exc:
        # ID is not here, there’s nothing we can do
        raise ValueError("The input file needs to contain key 'ID'.") from exc


    # read the salts, if they are present
    try:
        df = pd.read_csv(
            path,
            sep=None,
            engine="python",
            usecols=["crypto_salt"],
            keep_default_na=False,
            dtype=str)
        
        input_df["crypto_salt"] = df["crypto_salt"]
        logger.info("The original file provides a column for cryptographic salts.")
    except ValueError as exc:
        # salt is not here, fill in with an empty string as placeholder
        input_df["crypto_salt"] = ""
        logger.info("The original file DOES NOT provide cryptographic salts.")
    
    # drop duplicate values
    input_df.drop_duplicates(subset="ID", keep="first", inplace=True)

    # fill out empty salts
    ids_to_fill = input_df["crypto_salt"].str.len() == 0
    if ids_to_fill.any():
        Ntot = len(ids_to_fill)
        Nempty = sum(ids_to_fill)
        logger.info(f"A random salt will be generated for {Nempty}/{Ntot} values ({100*Nempty/Ntot:2.1f}%).")
    input_df.loc[ids_to_fill, "crypto_salt"] = [rand_salt() for _ in range(sum(ids_to_fill))]

    return input_df



def mask_ids(
        path: str,
        passwd: str,
        *,
        path_out: Optional[str] = None,
        N_iter: int = DEFAULT_HMAC_ITERATIONS
    ):

    if path_out is None:
        path_out = os.path.join(os.path.dirname(path), "masking_table.csv")

    df_out = _parse_data_file(path)


    def _hashfun(salt, id):
        return hashlib.pbkdf2_hmac(
            hash_name="sha256",
            password=bytes(passwd, encoding="utf8"),
            salt=bytes.fromhex(salt)+bytes(id, encoding="utf8"),
            iterations=N_iter).hex()
        
    df_out["ID_masked"] = df_out.apply(lambda ser: _hashfun(ser["crypto_salt"], ser["ID"]), axis="columns")

    df_out.to_csv(path_out, index=False)
    logger.info(f"Masking table saved as {path_out}.")


if __name__=='__main__':

    args = _parse_args()
    path = args.id_file
    path_out = args.out
    N_iter = args.N_iter

    passwd = getpass.getpass(prompt="Masking password:")
    if not passwd:
        logger.warn("Empty password. Make sure to read and understand the relevant section of the readme file before using the resulting table.")

    

    mask_ids(path, passwd, N_iter=N_iter, path_out=path_out)
